//
// Created by Phoebe Mitchell on 04/04/2023.
//

#include <stdio.h>
#include <strings.h>
#include "src/GraphLoading.h"

//int main (int argv, char *argv[]) {
//    FILE *file = fopen(argv[1], "r");
//    printf("%s\n", argv[1]);
//    fflush(stdout);
//    fscanf(file, "version 1\n");
//    char map[256];
//    int width, height, startX, startY, targetX, targetY;
//    while (fscanf(file, "%*i\t%s\t%i\t%i\t%i\t%i\t%i\t%i\t%*f\n", map, &width, &height, &startX, &startY, &targetX, &targetY) != EOF);
//
//    strcat(map, argv[2]);
//    char path[256];
//    strcat(path, argv[2]);
//    strcat(path, map);
//    FILE *graph;
//    GraphLoading_LoadGraph(path, graph);
//
//    NodeList nodeList;
//    GraphLoading_ParseGraphStandard(&nodeList, graph);
//    fclose(graph);
//
//    NodeIDList nodeIdList = PlatformerPathfinding_FindPath(&nodeList, height * startY + startX, height * targetY + targetX);
//    float sum = 0;
//    for (int i = 0; i < nodeIdList.count; i++) {
//        sum++;
//    }
//    printf("%f\n", sum);
//}
