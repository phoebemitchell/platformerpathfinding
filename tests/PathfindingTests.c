//
// Created by Phoebe Mitchell on 12/01/2023.
//

#include <string.h>
#include "unity.h"
#include "../src/PlatformerPathfinding.h"
#include "../src/GraphLoading.h"

FILE *LoadTestGraph(char *graphName) {
    char graphPath[256] = "../../tests/test_data/";
    strcat(graphPath, graphName);
    FILE *file;
    TEST_ASSERT_EQUAL(0, GraphLoading_LoadGraph(graphPath, &file));
    return file;
}

void test_FindPathTopDown() {
    FILE *exampleGraph = LoadTestGraph("ExampleGraph.mesh");
    const int expectedPath[] = {32, 40, 41, 42, 43, 35, 27, 28, 20, 12, 11, 10, 9, 8};
    const int case2_expectedPath[] = {32, 40, 41, 42, 43, 44, 45, 46, 47, 39, 31, 23, 15, 7};

    NodeList nodeList;
    GraphLoading_ParseGraph(&nodeList, exampleGraph);
    NodeIDList actualPath = PlatformerPathfinding_FindPath(&nodeList, 32, 8, TopDown, 0);
    TEST_ASSERT_EQUAL_INT_ARRAY(expectedPath, actualPath.nodeIDs, actualPath.count);
    Node_FreeNodeIDList(&actualPath);
    Node_FreeNodeList(&nodeList);

    NodeList nodeList2;
    GraphLoading_ParseGraph(&nodeList2, exampleGraph);
    actualPath = PlatformerPathfinding_FindPath(&nodeList2, 32, 7, TopDown, 0);
    TEST_ASSERT_EQUAL_INT_ARRAY(case2_expectedPath, actualPath.nodeIDs, actualPath.count);
    fclose(exampleGraph);
    Node_FreeNodeIDList(&actualPath);
    Node_FreeNodeList(&nodeList2);
}

void test_FindPathPlatformer() {
    FILE *exampleGraph = LoadTestGraph("ExampleGraph.mesh");
    const int expectedPath[] = {32, 40, 41, 42, 43, 35, 27, 28, 20, 12, 11, 10, 9, 8};
    const int case2_expectedPath[] = {32, 40, 41, 42, 43, 35, 27, 28, 29, 21, 13, 14, 15, 7};

    NodeList nodeList;
    GraphLoading_ParseGraph(&nodeList, exampleGraph);
    NodeIDList actualPath = PlatformerPathfinding_FindPath(&nodeList, 32, 8, Platformer, 3);
    TEST_ASSERT_EQUAL_INT_ARRAY(expectedPath, actualPath.nodeIDs, actualPath.count);
    Node_FreeNodeIDList(&actualPath);

    Node_FreeNodeIDList(&nodeList);

    NodeList nodeList2;
    GraphLoading_ParseGraph(&nodeList2, exampleGraph);
    actualPath = PlatformerPathfinding_FindPath(&nodeList2, 32, 7, Platformer, 3);
    TEST_ASSERT_EQUAL_INT_ARRAY(case2_expectedPath, actualPath.nodeIDs, actualPath.count);
    fclose(exampleGraph);
    Node_FreeNodeIDList(&actualPath);
    Node_FreeNodeList(&nodeList2);
}

void TestGraph(char *graphName, const int *expectedPath, const int expectedPathLength, const int start, const int target) {
    FILE *graph = LoadTestGraph(graphName);

    NodeList nodeListLarge;
    GraphLoading_ParseGraph(&nodeListLarge, graph);
    NodeIDList actualPath = PlatformerPathfinding_FindPath(&nodeListLarge, start, target, TopDown, 0);
    TEST_ASSERT_EQUAL_INT_ARRAY(expectedPath, actualPath.nodeIDs, expectedPathLength);

    fclose(graph);
    Node_FreeNodeList(&nodeListLarge);
    Node_FreeNodeIDList(&actualPath);
}

void test_CanTraverseLargeGraph() {
    const int expectedPath[] = {0, 44, 88, 132, 176, 220, 264, 308, 352, 396, 440, 484, 528, 572, 616, 660, 704, 748, 792, 836, 880, 924, 968, 1012, 1056, 1100, 1144, 1188, 1189, 1190, 1191, 1192, 1193, 1194, 1195, 1196, 1197, 1198, 1199, 1200, 1201, 1202, 1203, 1204, 1205, 1206, 1207, 1208, 1209, 1210, 1211, 1212, 1213, 1214, 1215, 1216, 1217, 1218, 1219, 1220, 1221, 1222, 1223, 1224, 1225, 1226, 1227, 1228, 1229, 1230, 1231};
    TestGraph("LargeGraph.mesh", expectedPath, 71, 0, 28 * 44 - 1);
}

void test_CanTraverseExtraLargeGraph() {
    const int expectedPath[] = {0, 88, 176, 264, 352, 440, 528, 616, 704, 792, 880, 968, 1056, 1144, 1232, 1320, 1408, 1496, 1584, 1672, 1760, 1848, 1936, 2024, 2112, 2200, 2288, 2376, 2464, 2552, 2640, 2728, 2816, 2904, 2992, 3080, 3168, 3256, 3344, 3432, 3520, 3608, 3696, 3784, 3872, 3960, 4048, 4136, 4224, 4312, 4400, 4488, 4576, 4664, 4752, 4840, 4841, 4842, 4843, 4844, 4845, 4846, 4847, 4848, 4849, 4850, 4851, 4852, 4853, 4854, 4855, 4856, 4857, 4858, 4859, 4860, 4861, 4862, 4863, 4864, 4865, 4866, 4867, 4868, 4869, 4870, 4871, 4872, 4873, 4874, 4875, 4876, 4877, 4878, 4879, 4880, 4881, 4882, 4883, 4884, 4885, 4886, 4887, 4888, 4889, 4890, 4891, 4892, 4893, 4894, 4895, 4896, 4897, 4898, 4899, 4900, 4901, 4902, 4903, 4904, 4905, 4906, 4907, 4908, 4909, 4910, 4911, 4912, 4913, 4914, 4915, 4916, 4917, 4918, 4919, 4920, 4921, 4922, 4923, 4924, 4925, 4926, 4927};
    TestGraph("ExtraLargeGraph.mesh", expectedPath, 71 * 2, 0, 28 * 2 * 44 * 2 - 1);
}

void setUp() {

}

void tearDown() {

}

int main() {
    UNITY_BEGIN();
    RUN_TEST(test_FindPathTopDown);
    RUN_TEST(test_FindPathPlatformer);
    RUN_TEST(test_CanTraverseLargeGraph);
    RUN_TEST(test_CanTraverseExtraLargeGraph);
    return UNITY_END();
}
