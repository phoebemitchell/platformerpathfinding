//
// Created by Phoebe Mitchell on 03/0'1''/2023.
//

#include "unity.h"
#include "../src/GraphLoading.h"

FILE *exampleGraph;
NodeList nodeList;

char exampleGraphArray[] = {
        '0','0','0','0','0','0','0','0',
        '1','1','1','1','0','0','1','0',
        'g','g','g','g','0','0','g','0',
        '0','0','0','0','1','1','1','0',
        '0','0','0','0','g','g','g','0',
        '1','1','1','1','1','1','1','1',
        'g','g','g','g','g','g','g','g'
};

void test_GraphLoadsCorrectly() {
    int character;
    int arrayPosition = 0;
    while ((character = fgetc(exampleGraph)) != EOF) {
        if (character == ',' || character == '\n') {
            continue;
        }

        TEST_ASSERT_EQUAL(exampleGraphArray[arrayPosition++], character);
    }
    fclose(exampleGraph);
}

void test_ParseGraph() {
    int arrayPosition = 0;
    for (int i = 0; i < nodeList.count; i++) {
        NodeType nodeType = nodeList.nodes[i].nodeType;
        switch (exampleGraphArray[arrayPosition++]) {
            case '0':
                TEST_ASSERT_EQUAL(air, nodeType);
                break;
            case '1':
                TEST_ASSERT_EQUAL(traversable, nodeType);
                break;
            case 'g':
                TEST_ASSERT_EQUAL(ground, nodeType);
                break;
        }
    }
    fclose(exampleGraph);
}

int exampleConnectionArray[][4] = {
        // Row 1
        {8,  1},
        {9, 2, 0},
        {10, 3, 1},
        {11, 4, 2},
        {12, 5, 3},
        {13, 6, 4},
        {14, 7, 5},
        {15, 6},

        // Row 2
        {9,  0},
        {10, 1, 8},
        {11,2, 9},
        {12, 3, 10},
        {20, 13, 4, 11},
        {21, 14, 5, 12},
        {15, 6, 13},
        {23, 7, 14},

        // Row 3
        {},
        {},
        {},
        {},
        {28, 21, 12},
        {29, 13, 20},
        {},
        {31, 15,},

        // Row 4
        {32, 25},
        {33, 26, 24},
        {34, 27, 25},
        {35, 28, 26},
        {29, 20,27},
        { 30, 21,28},
        {31, 29},
        {39, 23, 30},

        // Row 5
        {40, 33, 24},
        {41, 34, 25, 32},
        {42, 35, 26, 33},
        {43, 27, 34},
        {},
        {},
        {},
        {47, 31},

        {41, 32},
        {42, 33, 40},
        { 43, 34,41},
        { 44, 35,42},
        {45, 43},
        {46, 44},
        {47, 45},
        {39, 46},

        {},
        {},
        {},
        {},
        {},
        {},
        {}
};

void test_CreateConnections() {
    NodeList nodeList;
    GraphLoading_ParseGraph(&nodeList, exampleGraph);
    for (int i = 0; i < nodeList.count; i++) {
        Node *node = &nodeList.nodes[i];
        for (int j = 0; j < node->connectionCount; j++) {
            TEST_ASSERT_EQUAL(exampleConnectionArray[i][j], node->connections[j].nodeId);
        }
    }
}

void setUp() {
    TEST_ASSERT_TRUE(GraphLoading_LoadGraph("../../tests/test_data/ExampleGraph.mesh", &exampleGraph) == 0);
    GraphLoading_ParseGraph(&nodeList, exampleGraph);
}

void tearDown() {
    fclose(exampleGraph);
    Node_FreeNodeList(&nodeList);
}

int main() {
    UNITY_BEGIN();
    RUN_TEST(test_GraphLoadsCorrectly);
    RUN_TEST(test_ParseGraph);
    RUN_TEST(test_CreateConnections);
    return UNITY_END();
}
