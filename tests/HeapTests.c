//
// Created by Phoebe Mitchell on 20/03/2023.
//

#include "unity.h"
#include "../src/Heap.h"

Heap heap;

void test_InsertToHeap() {
    Node node1;
    node1.f = 1;
    Heap_Insert(&heap, &node1);
    Node *expected1[] = {&node1};
    TEST_ASSERT_EQUAL_PTR_ARRAY(expected1, heap.nodes, 1);

    Node node2;
    node2.f = 2;
    Heap_Insert(&heap, &node2);
    Node *expected2[] = {&node1, &node2};
    TEST_ASSERT_EQUAL_PTR_ARRAY(expected2, heap.nodes, 2);

    Node node3;
    node3.f = 3;
    Heap_Insert(&heap, &node3);
    Node *expected3[] = {&node1, &node2, &node3};
    TEST_ASSERT_EQUAL_PTR_ARRAY(expected3, heap.nodes, 3);

    Node node4;
    node4.f = 1.5;
    Heap_Insert(&heap, &node4);
    Node *expected4[] = {&node1, &node4, &node3, &node2};
    TEST_ASSERT_EQUAL_PTR_ARRAY(expected4, heap.nodes, 4);

    Node node5;
    node5.f = 0.5;
    Heap_Insert(&heap, &node5);
    Node *expected5[] = {&node5, &node1, &node3, &node2, &node4};
    TEST_ASSERT_EQUAL_PTR_ARRAY(expected5, heap.nodes, 5);
}

void test_DeleteFromHeap() {
    const int NODE_COUNT = 5;
    Node nodes[NODE_COUNT];
    for (int i = 0; i < NODE_COUNT; i++) {
        nodes[i].f = i;
        Heap_Insert(&heap, &nodes[i]);
    }

    float expected1[] = {0, 1 ,2 ,3 ,4};
    float expected2[] = {1, 3, 2, 4};
    float expected3[] = {2, 3, 4};
    float expected4[] = {3, 4};
    float expected5[] = {4};

    for (int i = 0; i < 5; i++) {
        TEST_ASSERT_EQUAL_FLOAT(expected1[i], heap.nodes[i]->f);
    }
    TEST_ASSERT_EQUAL_PTR(&nodes[0], Heap_Delete(&heap));

    for (int i = 0; i < 4; i++) {
        TEST_ASSERT_EQUAL_FLOAT(expected2[i], heap.nodes[i]->f);
    }
    TEST_ASSERT_EQUAL_PTR(&nodes[1], Heap_Delete(&heap));

    for (int i = 0; i < 3; i++) {
        TEST_ASSERT_EQUAL_FLOAT(expected3[i], heap.nodes[i]->f);
    }
    TEST_ASSERT_EQUAL_PTR(&nodes[2], Heap_Delete(&heap));

    for (int i = 0; i < 2; i++) {
        TEST_ASSERT_EQUAL_FLOAT(expected4[i], heap.nodes[i]->f);
    }
    TEST_ASSERT_EQUAL_PTR(&nodes[3], Heap_Delete(&heap));

    for (int i = 0; i < 1; i++) {
        TEST_ASSERT_EQUAL_FLOAT(expected5[i], heap.nodes[i]->f);
    }
    TEST_ASSERT_EQUAL_PTR(&nodes[4], Heap_Delete(&heap));
}

void setUp() {
    //heap = Heap_CreateHeap();
}

void tearDown() {
    Heap_Free(&heap);
}

int main() {
    UNITY_BEGIN();
//    RUN_TEST(test_InsertToHeap);
//    RUN_TEST(test_DeleteFromHeap);
    return UNITY_END();
}
