//
// Created by Phoebe Mitchell on 23/03/2023.
//

#ifndef PLATFORMERPATHFINDINGIMPLEMENTATION_HEAP_H
#define PLATFORMERPATHFINDINGIMPLEMENTATION_HEAP_H

#include "Node.h"

typedef struct {
    Node **nodes;
    int size;
} Heap;

Heap Heap_Create();
void Heap_Insert(Heap *heap, Node *node);
Node *Heap_Delete(Heap *heap);
void Heap_Update(Heap *heap, int position, float oldF);
void Heap_Free(Heap *heap);


#endif //PLATFORMERPATHFINDINGIMPLEMENTATION_HEAP_H
