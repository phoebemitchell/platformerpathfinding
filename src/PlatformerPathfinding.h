//
// Created by Phoebe Mitchell on 05/01/2023.
//

#ifndef PLATFORMERPATHFINDING_PLATFORMERPATHFINDING_H
#define PLATFORMERPATHFINDING_PLATFORMERPATHFINDING_H

#include "Node.h"

typedef enum {
    TopDown,
    Platformer
} PathType;

NodeIDList PlatformerPathfinding_FindPath(NodeList *nodeList, int start, int target, PathType pathType, int maxJumpTime);

#endif //PLATFORMERPATHFINDING_PLATFORMERPATHFINDING_H
