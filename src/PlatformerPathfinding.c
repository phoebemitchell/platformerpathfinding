//
// Created by Phoebe Mitchell on 05/01/2023.
//

#include <stdlib.h>
#include <math.h>
#include <printf.h>
#include "PlatformerPathfinding.h"
#include "Node.h"
#include "Heap.h"

// Finds a path from a given start node to a target node in a node list
NodeIDList PlatformerPathfinding_FindPath(NodeList *nodeList, int start, int target, PathType pathType, int maxJumpTime) {
    // Set g and f values for all nodes to infinity
    for (int i = 0; i < nodeList->count; i++) {
        nodeList->nodes[i].id = i;
        nodeList->nodes[i].f = INFINITY;
        nodeList->nodes[i].g = INFINITY;
        nodeList->nodes[i].parentId = -1;
        nodeList->nodes[i].airTime = 0;
        nodeList->nodes[i].incomingConnection = NULL;
        for (int j = 0; j < nodeList->nodes[i].connectionCount; j++) {
            nodeList->nodes[i].connections[j].airUpTime = 0;
        }
        nodeList->nodes[i].closed = 0;
        nodeList->nodes[i].falling = 0;
    }

    // Create open and closed lists
    Heap open = Heap_Create();
    NodeIDList closed = Node_CreateNodeIDList();

    // Initialise start node by adding it to open list and setting its f value to 0
    Heap_Insert(&open, &nodeList->nodes[start]);
    float oldF = nodeList->nodes[start].f;
    nodeList->nodes[start].f = 0;
    Heap_Update(&open, nodeList->nodes[start].heapPosition, oldF);
    nodeList->nodes[start].g = 0;

    Node *targetNode = &nodeList->nodes[target];
    // Main algorithm loop
    while (open.size > 0) {
        // Get ID of node with lowest f value
        Node *n = Heap_Delete(&open);
        int currentNodeId = n->id;
        Node *currentNode = &nodeList->nodes[currentNodeId];

        // If current node is the target node then we are done
        if (currentNodeId == target) {
            NodeIDList path = Node_CreateNodeIDList();
            int id = target;
            while (id != start) {
                Node_AddToNodeIdList(&path, id);
                path.cost += nodeList->nodes[id].cost;
                id = nodeList->nodes[id].parentId;
                if (id == -1) {
                    return (NodeIDList){0,-1};
                }
            }
            Node_AddToNodeIdList(&path, start);
            Node_ReverseNodeIdList(&path);
            Heap_Free(&open);
            free(closed.nodeIDs);
            return path;
        }

        // Loop through each neighbour
        for (int i = 0; i < currentNode->connectionCount; i++) {
            Connection *connection = &currentNode->connections[i];
            int neighbourID = connection->nodeId;
            Node *neighbour = &nodeList->nodes[neighbourID];

            if (neighbourID == currentNode->parentId) {
                continue;
            }

            // If already in the closed list
            if (neighbour->closed) {
                continue;
            }

            if (pathType == Platformer) {
                if (neighbour->nodeType == air) {
                    neighbour->airTime = currentNode->airTime + 1;
                }

                if (neighbour->nodeType == air && neighbour->row <= currentNode->row) {
                    if (currentNode->incomingConnection != NULL) {
                        connection->airUpTime = currentNode->incomingConnection->airUpTime + 1;
                    } else {
                        connection->airUpTime = 1;
                    }
                }

                if (connection->airUpTime > maxJumpTime) {
                    continue;
                }

                neighbour->falling = (neighbour->nodeType == air && neighbour->row > currentNode->row) ||
                                     (neighbour->nodeType == air && currentNode->falling);

                if (neighbour->nodeType == air && neighbour->row < currentNode->row && currentNode->falling) {
                    continue;
                }

                if (neighbour->nodeType == air && neighbour->airTime % 2 == 0 && neighbour->row == currentNode->row) {
                    continue;
                }
            }

            // If neighbour is not in open list add it
            if (neighbour->heapPosition == -1) {
                Heap_Insert(&open, neighbour);
            }

            // Calculate g, h, and f scores
            float cost = currentNode->connections[i].cost;
            if (neighbour->row > currentNode->row) {
                cost = 1;
            }

            float g = currentNode->g + cost;
            float h = abs(targetNode->row - neighbour->row) + abs(targetNode->column - neighbour->column);
            float f = g + h;

            // If calculated f score is better update f, g, and parentID values of neighbour
            if (f < neighbour->f) {
                float oldF = neighbour->f;
                neighbour->f = f;
                neighbour->g = g;
                Heap_Update(&open, neighbour->heapPosition, oldF);
                neighbour->parentId = currentNodeId;
                neighbour->incomingConnection = connection;
            }
        }

        Node_AddToNodeIdList(&closed, currentNodeId);
        currentNode->closed = 1;
    }

    Heap_Free(&open);
    free(closed.nodeIDs);

    return (NodeIDList){0,-1};
}
