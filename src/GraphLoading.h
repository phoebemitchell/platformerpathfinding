//
// Created by Phoebe Mitchell on 05/01/2023.
//

#ifndef PLATFORMERPATHFINDING_GRAPHLOADING_H
#define PLATFORMERPATHFINDING_GRAPHLOADING_H

#include "PlatformerPathfinding.h"

#define GROUND_CHAR 'g'
#define AIR_CHAR '0'
#define TRAVERSABLE_CHAR '1'

int GraphLoading_LoadGraph(const char *path, FILE **file);
void GraphLoading_ParseGraph(NodeList *nodeList, FILE *file);

#endif //PLATFORMERPATHFINDING_GRAPHLOADING_H
