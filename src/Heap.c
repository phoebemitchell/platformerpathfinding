//
// Created by Phoebe Mitchell on 23/03/2023.
//

#include <stdlib.h>
#include "Heap.h"
#include "math.h"

#define Parent(x) (int)floor((x - 1) / 2)
#define Child(x) 2 * x + 1

// Creates a heap data structure by allocating memory and initialising all variables in the heap
Heap Heap_Create() {
    Heap heap;
    heap.nodes = malloc(0);
    heap.size = 0;
    return heap;
}

// Swaps elements at two given positions in a heap
void Heap_Swap(Heap *heap, int a, int b) {
    heap->nodes[b]->heapPosition = a;
    heap->nodes[a]->heapPosition = b;

    Node *temp = heap->nodes[a];
    heap->nodes[a] = heap->nodes[b];
    heap->nodes[b] = temp;
}

// Heapify operation, going up
void Heap_HeapifyUp(Heap *heap, int position) {
    int parentPosition = Parent(position);

    if (parentPosition > heap->size - 1) {
        return;
    }

    if (heap->nodes[position]->f < heap->nodes[parentPosition]->f) {
        Heap_Swap(heap, position, parentPosition);
        Heap_HeapifyUp(heap, parentPosition);
    }
}

// Heapify operation, going down
void Heap_HeapifyDown(Heap *heap, int position) {
    // Calculate the left and right child positions
    int leftPosition = Child(position);
    int rightPosition = leftPosition + 1;

    // Declare variables for left and right children
    Node *left = NULL;
    Node *right = NULL;

    // Declare variables for smallest and largest position, assuming left is smallest
    int smallestPosition = leftPosition;
    int largestPosition = rightPosition;

    // Get left child node, or return if no node
    if (leftPosition < heap->size) {
        left = heap->nodes[leftPosition];
    } else {
        // No more values in heap
        return;
    }

    // Get right child node if it exists
    if (rightPosition < heap->size) {
        right = heap->nodes[rightPosition];
    }

    // Declare variables for smallest child and largest child, assuming left is smallest
    Node *smallestChild = left;
    Node *largestChild = right;

    // If neither child is null, check which one is smaller and swap them if needed
    if (left != NULL && right != NULL) {
        if (left->f > right->f ) {
            smallestChild = right;
            smallestPosition = rightPosition;
            largestChild = left;
            largestPosition = leftPosition;
        }
    }

    // Compare the node to the smallest child
    if (heap->nodes[position]->f > smallestChild->f) {
        Heap_Swap(heap, position, smallestPosition);
        Heap_HeapifyDown(heap, smallestPosition);
        return;
    }

    // Compare the node to the largest child
    if (largestChild != NULL && heap->nodes[position]->f > largestChild->f) {
        Heap_Swap(heap, position, largestPosition);
        Heap_HeapifyDown(heap, largestPosition);
        return;
    }
}

// Inserts a node at the end of a heap
// Allocates memory and increases size of heap
void Heap_Insert(Heap *heap, Node *node) {
    heap->size++;
    heap->nodes = realloc(heap->nodes, sizeof(Node*) * heap->size);
    heap->nodes[heap->size - 1] = node;
    Heap_HeapifyUp(heap, heap->size - 1);
    heap->nodes[heap->size - 1]->heapPosition = heap->size - 1;
}

// Deletes the first node in a heap by swapping it with the last value in the heap
// then removing the new last value from the heap
Node *Heap_Delete(Heap *heap) {
    Node *root = heap->nodes[0];
    Heap_Swap(heap, 0, heap->size - 1);
    heap->size--;
    heap->nodes = realloc(heap->nodes, sizeof(Node*) * heap->size);
    Heap_HeapifyDown(heap, 0);
    return root;
}

// Updates the value of a node in the heap
void Heap_Update(Heap *heap, int position, float oldF) {
    float f = heap->nodes[position]->f;
    if (oldF < f) {
        Heap_HeapifyDown(heap, position);
    } else if (oldF > f) {
        Heap_HeapifyUp(heap, position);
    }
}

// Frees memory allocated for a heap
void Heap_Free(Heap *heap) {
    free(heap->nodes);
}