//
// Created by Phoebe Mitchell on 02/02/2023.
//

#ifndef PLATFORMERPATHFINDING_NODE_H
#define PLATFORMERPATHFINDING_NODE_H

typedef enum {
    Walk,
    Jump
} MovementType;

typedef struct {
    int nodeId;
    float cost;
    MovementType movementType;
    int airUpTime;
} Connection;

typedef enum {
    air,
    ground,
    traversable
} NodeType;

typedef struct {
    int id;
    Connection *connections;
    int connectionCount;
    NodeType nodeType;
    int row;
    int column;
    float f;
    float g;
    int parentId;
    int airTime;
    int heapPosition;
    char closed;
    float cost;
    char falling;
    Connection *incomingConnection;
} Node;

typedef struct {
    Node *nodes;
    int count;
} NodeList;

// Structure to store a list of node IDs and its count
typedef struct {
    int *nodeIDs;
    int count;
    float cost;
} NodeIDList;

NodeIDList Node_CreateNodeIDList();
void Node_AddToNodeIdList(NodeIDList *nodeIDList, int nodeID);
void Node_RemoveFromNodeIDList(NodeIDList *nodeIDList, int nodeID);
char Node_NodeIDListContains(NodeIDList *nodeIDList, int nodeID);
void Node_ReverseNodeIdList(NodeIDList *nodeIdList);
void Node_CreateConnectionsList(Node *node);
void Node_AddConnection(Node *node, int connectionId, float cost, MovementType movementType);
void Node_InitialiseNodeList(NodeList *nodeList);
Node *Node_AddNewNodeToNodeList(NodeList *nodeList);
void Node_FreeNodeList(NodeList *nodeList);
void Node_FreeNodeIDList(NodeIDList *nodeIdList);

#endif //PLATFORMERPATHFINDING_NODE_H
