//
// Created by Phoebe Mitchell on 05/01/2023.
//

#include <stdio.h>
#include <stdlib.h>
#include "GraphLoading.h"

const int JUMP_COST = 2;

// Load a graph from a file and store it in a provided FILE variable
// Returns 0 if success
// Returns -1 if failure
int GraphLoading_LoadGraph(const char *path, FILE **file) {
    FILE *f = fopen(path, "r");
    if (f == NULL) {
       printf("File could not be loaded at path \"%s\".\n", path);
       return -1;
    }
    *file = f;
    return 0;
}

// Converts a 2D grid position to a position in a list
int GraphLoading_GetNodeIndexAtPosition(int row, int column, int width, int height) {
    if (column < 0 || column >= width ||
        row < 0 || row >= height) {
        return -1;
    }

    int targetRowStartIndex = row * width;
    return column + targetRowStartIndex;
}

// Adds a connection from a source node to the target node
void GraphLoading_AddConnectionToNode(NodeList *nodeList, Node *sourceNode, int horizontalOffset, int verticalOffset, int row, int offset, int width, int height, MovementType movementType, int cost) {
    // Convert 2D grid position to position in list
    int targetOffset = offset + horizontalOffset;
    int targetRow = row + verticalOffset;
    int targetIndex = GraphLoading_GetNodeIndexAtPosition(targetRow, targetOffset, width, height);

    if (targetIndex == -1) {
        return;
    }

    // Connect the source node and the target node
    Node *targetNode = &nodeList->nodes[targetIndex];

    if (targetNode->nodeType == air) {
        cost = 1000;
    }

    if (targetNode->nodeType == ground) {
        return;
    }
    Node_AddConnection(sourceNode, targetIndex, cost, movementType);
}

// Loops through every node in a list and creates connections to adjacent nodes
void GraphLoading_CreateConnections(NodeList *nodeList, int width, int height) {
    for (int i = 0; i < nodeList->count; i++) {
        Node *node = &nodeList->nodes[i];
        Node_CreateConnectionsList(node);

        if (node->nodeType == ground) {
            continue;
        }

        int row = i / width;
        node->row = row;
        int rowStartIndex = row * width;
        int column = i - rowStartIndex;
        node->column = column;

        GraphLoading_AddConnectionToNode(nodeList, node, 0, 1, row, column, width, height, Walk, 1);
        GraphLoading_AddConnectionToNode(nodeList, node, 1, 0, row, column, width, height, Jump, JUMP_COST);
        GraphLoading_AddConnectionToNode(nodeList, node, 0, -1, row, column, width, height, Jump, JUMP_COST);
        GraphLoading_AddConnectionToNode(nodeList, node, -1, 0, row, column, width, height, Walk, 1);
    }
}

// Takes a graph FILE variable and converts it to a NodeList structure
void GraphLoading_ParseGraph(NodeList *nodeList, FILE *file) {
    rewind(file);
    // Initialise variables
    int character;
    nodeList->count = 0;
    int width = 0;
    int height = 1;
    char widthSet = 0;
    Node_InitialiseNodeList(nodeList);

    // Read file and create list of nodes with types
    while ((character = fgetc(file)) != EOF) {
        if (character == ',') {
            continue;
        } else if (character == '\n') {
            widthSet = 1;
            height++;
            continue;
        }

        Node *node = Node_AddNewNodeToNodeList(nodeList);
        switch (character) {
            case AIR_CHAR:
                node->nodeType = air;
                break;
            case GROUND_CHAR:
                node->nodeType = ground;
                break;
            case TRAVERSABLE_CHAR:
                node->nodeType = traversable;
                break;
        }
        if (!widthSet) {
            width++;
        }
    }

    GraphLoading_CreateConnections(nodeList, width, height);
}
