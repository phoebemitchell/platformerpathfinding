# Platformer Pathfinding
This is a library for pathfinding in 2D platformers.

# Build Instructions
Use the following commands to build the project.
```
mkdir PlaterformerBuild
cd PlatformerBuild
cmake ..
make
```

# Style Guide

This document details the code style used for this project.

## Naming
- Local variables: Camel case, e.g. camelCase
- Constants/Macros: Screaming snake case, e.g. SCREAMING\_SNAKE\_CASE
- Functions: Source file name followed by an underscore and the name of the function in pascal case e.g. SourceFileName\_PascalCase
- Structs: Pascal case, e.g. PascalCase
	- Structs should be named using typedef, e.g.
	```
	typedef struct { // struct contents } StructName;
	```
- Struct fields: Camel case, e.g. camelCase
- Source files: Pascal case followed by ".c", e.g. PascalCase.c
- Header files: Pascal case followed by ".h", e.g. PascalCase.h
- Directories: Snake case, e.g. snake\_case
- C++ only:
	- Class name: Pascal case, e.g. PascalCase
	- Private fields: Camel case preceeded by "m\_", e.g. m\_camelCase


## Comments
- All functions should be preceeded by a comment explaining what it does and why it does it
- Otherwise comments should be used sparingly

## Practices
- Do not use global variables
- Do not use static variables
- C++ only:
	- Do not use public fields in classes. Getter and setter methods should be used instead.